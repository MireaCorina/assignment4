import React, {Component} from 'react'

class RobotForm extends Component {
    constructor(props) {
        super(props)
        this.state = {
            name : '',
            type : '',
            mass : ''
        }

        this.handleChange = (evt) => {
            this.setState({
                [evt.target.name] : evt.target.value
            })
        }
        
        this.add = () => {
            this.props.onAdd({
                name : this.state.name,
                type : this.state.type,
                mass : this.state.mass
            })
        }

        this.addd = () => {
            console.log(this.state.name)
            console.log(this.state.type)
            console.log(this.state.mass)
            
        }
    }
    
    render() {
        return <div>
            <input type="text" placeholder="name" name="name" id="name" onChange={this.handleChange} />
            <input type="text" placeholder="type" name="type" id="type" onChange={this.handleChange} />
            <input type="text" placeholder="mass" name="mass" id="mass" onChange={this.handleChange} />
            <input type="button" value="add" onClick={this.add} />
            <input type="button" value="addd" onClick={this.addd} />
        </div>
    }
}

export default RobotForm